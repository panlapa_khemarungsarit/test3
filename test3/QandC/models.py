from django.db import models

class Question(models.Model):
    q = models.TextField(default='')
    date = models.TextField(default='')

class Choice(models.Model):
    que = models.ForeignKey(Question)
    c = models.TextField(default='')

class Answer(models.Model):
    date = models.TextField(default='')
    que = models.ForeignKey(Question)
    ans = models.TextField(default='')
