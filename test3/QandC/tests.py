from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
from QandC.models import Quation, Choose, Answer
from QandC.views import home, make, cho, ans, ansQ

class HomeTest(TestCase):

    def test_make_q(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['q_text'] = 'เดือนเกิด'
        request.POST['send'] = 'send'
        response = make(request)
        request = HttpRequest()
        response = ans(request)
        self.assertIn('เดือนเกิด', response.content.decode())

    def test_make_c(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['q_text'] = 'เดือนเกิด'
        request.POST['send'] = 'send'
        response = make(request)
        request = HttpRequest()
        request.method = 'POST'
        request.POST['c_text'] = 'มกราคม'
        request.POST['send'] = 'send'
        response = cho(request, 1)
        request = HttpRequest()
        response = ansQ(request, 1)
        self.assertIn('มกราคม', response.content.decode())
        

        
