#!/usr/bin/python
from django.shortcuts import redirect, render
from QandC.models import Question, Choice,Answer
from django.http import HttpResponse
import operator
import pytz
from django.utils import timezone
import datetime


def home(request):
    if request.method == 'POST' and request.POST.get('q','') == 'q':
        return redirect('/make')
    if request.method == 'POST' and request.POST.get('c','') == 'c':
        return redirect('/answer')
    return render(request, 'home.html')

def make(request):
    if request.method == 'POST' and request.POST.get('send','') == 'send':
        q_text = request.POST['q_text']
        date_time = datetime.datetime.utcnow()
        Question.objects.create(
            q=q_text,
            date = str(date_time)
            )
        q = Question.objects.get(q=q_text)
        return redirect('/make/%d/' %(q.id)) 
    return render(request, 'make.html')

def choice(request, q_id): 
    q = Question.objects.get(pk=q_id)
    if request.method == 'POST' and request.POST.get('send','') == 'send':
        c_text = request.POST['c_text']
        Choice.objects.create(
            que=q,
            c = c_text
            )
        return redirect('/make/%d/' %(q.id))  
    if request.method == 'POST' and request.POST.get('finish','') == 'finish':
        return redirect('/')  
    return render(request, 'cho.html')

def ans(request):
    question = Question.objects.all()
    if request.method == 'POST' and request.POST.get('q','') == 'q':
        return redirect('/answer/%d/' %int(request.POST.get('id_question','')) )
    if request.method == 'POST' and request.POST.get('a','') == 'a':
        return redirect('/show/%d/' %int(request.POST.get('id_question','')) )  
    return render(request, 'ans.html', {'questions': question })

def ansQ(request, q_id):
    q = Question.objects.get(pk=q_id)
    if request.method == 'POST' and request.POST.get('send','') == 'send':
        c_text = request.POST.get('choice','')
        date_time = datetime.datetime.utcnow()
        Answer.objects.create(
            que=q,
            ans = c_text,
            date = str(date_time)
            )
        return redirect('/')
    return render(request, 'ansq.html', {'text_q': q.q , 'question': q })

def show(request, q_id):
    q = Question.objects.get(pk=q_id)
    ans = q.answer_set.all()
    list_ = []
    for a in ans:
        list_.append(a.ans)
    list_count = [[x,list_.count(x)] for x in set(list_)]
    return render(request, 'showans.html', {'text_q': q.q , 'ans': list_count})
    





